'''
Created on 20 mars 2020

@author: b.jacob
'''
import subprocess
import sys
from asyncio.subprocess import STDOUT
from time import sleep
from subprocess import Popen,PIPE
from com.bjacob.Orchestrator import Orchestrator
from com.bjacob.api.ApiLauncher import ApiLauncher
from backcall._signatures import Parameter
from sklearn import linear_model
import matplotlib.pyplot as plt
from sklearn.manifold import MDS
import csv
import numpy as np
from sklearn import tree
from sklearn.ensemble import GradientBoostingRegressor
from xgboost import XGBRegressor
import plotly
import plotly.graph_objs as go


class Parameter(object):
    def __init__(self,min,max,pas,name):
        self.min = min
        self.max = max
        self.pas = pas
        self.name = name;
        self.value = min
        
    def inc(self):
        self.value = min((self.value + self.pas),self.max)
        
    def init(self):
        self.value = self.min
        
    def isMax(self):
        return self.value == self.max
    
    def getValues(self):
        values = [self.value];
        while not self.isMax():
            self.inc()
            values.append(self.value)
        self.init()
        return values
        
class SystemModel:
    def __init__(self):
        self.X = []
        self.Y = []
        self.X0 = []
        self.Y0 = []
        self.minTime = 100000
        
    def split(self,nb):
        self.X0 = self.X[(self.X.__len__() - nb):self.X.__len__()]
        self.Y0 = self.Y[(self.Y.__len__() - nb):self.Y.__len__()]
        for i in range((self.X.__len__() - nb),self.X.__len__()):
            self.X.pop(self.X.__len__()-1)
            self.Y.pop(self.Y.__len__()-1)
    

class SystemRegressor(object):
    '''
    classdocs
    '''


    def __init__(self):
        '''
        Constructor
        '''
    
    def startSimulator(self,cpu,memory,disk):
        memoryMb = memory*1000000
        Popen(["powershell", "java -jar system-load-generator.jar "+str(cpu)+" "+str(cpu+10)+" "+str(memoryMb)+" "+str(disk)+" > simulator.log" ], shell=False)
        #ret = subprocess.check_output('powershell -ExecutionPolicy ByPass -Command java -jar system-load-generator.jar 40 50 2048000000 20',stderr=STDOUT).decode("utf-8"); 
        #ret = subprocess.check_output('powershell -ExecutionPolicy ByPass -Command java -version',stderr=STDOUT).decode("utf-8"); 
        print('simulator lancé ')
        
    def stopSimulator(self):
        p = subprocess.check_output('powershell -ExecutionPolicy ByPass -Command get-process java | stop-process')
        #p = Popen(["powershell", "get-process java | stop-process"], shell=True)
        print(p)
        
    def regressLinear(self):
        reg = linear_model.LinearRegression()
        model = self.getSystemModel()
        model.split(20)
        reg.fit(model.X, model.Y);
        print(model.X0)
        y1 = reg.predict(model.X0)
        print("coeff : ")
        print(reg.coef_)
        print("y1 = "+str(y1))
        mse = np.sum(abs(y1 - model.Y0)/model.Y0)
        rmse = (mse/model.Y0.__len__())*100
        print("error % : "+str(rmse))
        return model;
        
    def regressTree(self):
        reg = tree.DecisionTreeRegressor()
        model = self.getSystemModel()
        model.split(20)
        reg.fit(model.X, model.Y);
        print(model.X0)
        y1 = reg.predict(model.X0)
        
        print("y1 = "+str(y1))
        mse = np.sum(abs(y1 - model.Y0)/model.Y0)
        rmse = (mse/model.Y0.__len__())*100
        print("error % : "+str(rmse))
        return model;
        
    def regressGradientBoost(self):
        reg = GradientBoostingRegressor(n_estimators=100, learning_rate=0.1, max_depth=1, random_state=0, loss='lad')
        model = self.getSystemModel()
        model.split(200)
        reg.fit(model.X, model.Y);
        print(model.X0)
        y1 = reg.predict(model.X0)
        
        print("y1 = "+str(y1))
        mse = np.sum(abs(y1 - model.Y0)/model.Y0)
        rmse = (mse/model.Y0.__len__())*100
        print("error % : "+str(rmse))
        return model;
        
    def regressXGBoost(self):
        reg = XGBRegressor(objective="reg:squarederror")
        model = self.getSystemModel()
        model.split(200)
        reg.fit(model.X, model.Y);
        print(model.X0)
        y1 = reg.predict(model.X0)
        
        print("y1 = "+str(y1))
        mse = np.sum(abs(y1 - model.Y0)/model.Y0)
        rmse = (mse/model.Y0.__len__())*100
        print("error % : "+str(rmse))
        return model;
    
    def plot(self,model):
        cpuTab = []
        memoryTab=[]
        diskTab = []
        for vect in model.X:
            cpuTab.append(vect[0])
            memoryTab.append(vect[1])
            diskTab.append(vect[2])
            
        fig = go.Figure(data=[go.Scatter3d(x=cpuTab,
                  y=memoryTab,z=diskTab,mode='markers',
                  marker=dict(
        size=12,
        color=model.Y,
        colorscale='Viridis',   # choose a colorscale
        opacity=0.8
    )
                  )])
        
        fig.show()

        
    def getPercentLoss(self,cpu,memory,disk,plot=False):
        reg = GradientBoostingRegressor(n_estimators=100, learning_rate=0.1, max_depth=1, random_state=0, loss='lad')
        model = self.getSystemModel()
        model.split(200)
        reg.fit(model.X, model.Y);
        
        timePredicted = reg.predict([[cpu,memory,disk]])
        print("timePredicted : "+str(timePredicted))
        print("model.minTime : "+str(model.minTime))
        loss = (timePredicted - model.minTime)/timePredicted
        
        if plot:
            self.plot(model)
        
        return loss
        
    def getSystemModel(self):
        model = SystemModel();
        try :
            csvfile = open('traces/executions-regression-1000.csv')
        except:
            csvfile = open('../traces/executions-regression-1000.csv')
        
        readCSV = csv.reader(csvfile, delimiter=';')
        i = 0;
        for row in readCSV:
            if i>0 :
                time = round(float(row[8]),3)
                cpu = round(float(row[9]),3)
                memory = round(float(row[10]),3)
                disk = round(float(row[11]),3)
                model.Y.append(time)
                model.X.append([cpu,memory,disk])
                if model.minTime>time:
                    model.minTime = time
            i = i +1
        return model    
            
        
    def runCollect(self):
        o = Orchestrator()
        a = ApiLauncher()
        cpu = Parameter(20,80,5,'cpu')
        memory = Parameter(256,4096,64,'memory')
        disk = Parameter(15,50,5,'disk')
        
        cpuValues = cpu.getValues();
        memoryValues = memory.getValues();
        diskValues = disk.getValues();
        
        params = [cpu,memory,disk]
        valuesParams = {}
        
       
        for cpuVal in cpuValues:
            for memoryVal in memoryValues:
                for diskVal in diskValues:
                    print('cpu : '+str(cpuVal))
                    print('memory : '+str(memoryVal))
                    print('disk : '+str(diskVal))
                    print('-----------------------------------')
                    regressor.startSimulator(cpuVal,memoryVal,diskVal)
                    sleep(5)
                    a.runApi(stop=False)
                    sleep(10)
                    o.runClient()
                    try :
                        regressor.stopSimulator()
                        sleep(2)
                        a.stopApi()
                    except :
                        print("exception")
                    print("simulator stopped")
                    print('-----------------------------------<<<<<<<<<<<<<<<<<<<<<<<<<')
            
        
    
        
if __name__ == '__main__':
    regressor = SystemRegressor();
    
    #regressor.regressLinear()
    #regressor.regressTree()
    regressor.regressGradientBoost()
    #regressor.regressXGBoost()
    #regressor.run()
    loss = regressor.getPercentLoss(29.6,7379682.0,5.05,plot=True)#532.0138888888889
    print("loss : "+str(loss))
    
    sys.exit(0)
        