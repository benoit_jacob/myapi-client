#python -m pip install requests


import time
import configparser
from com.bjacob.MyThread import MyThread
from com.bjacob.monitoring.Monitoring import Monitoring
import requests


class MyApiClient:
    def __init__(self,nbThread,trace):
        self.nbThread = nbThread

        self.timeDict = {}

        self.batchSize = 1000

        self.nbCreates = 1
        self.trace = trace

    def getServerConf(self,pathConf):
        config = configparser.RawConfigParser()
        config.read(pathConf)
        print(config['DEFAULT']['spring.datasource.url'])
        return config;

    def traceExecution(self,delay,delayServer,cpu,mem,disk):
        f = open("traces/executions.csv", "a")
        sep = ';';
        config = self.getServerConf('./application.properties');
        line = str(time.time())+sep+str(self.batchSize)+sep+str(self.nbCreates)+sep+str(self.nbThread)+sep+str(config['DEFAULT']['spring.datasource.hikari.maximumPoolSize']);
        line = line + sep +str(config['DEFAULT']['spring.jpa.properties.hibernate.jdbc.batch_size'])
        line = line + sep +str(config['DEFAULT']['server.tomcat.max-threads'])
        line = line + sep +str(delay)+sep+str(delayServer)+sep+str(cpu)+sep+str(mem)+sep+str(disk)
        f.write(line+'\n')
        
    def getAverageTimeServer(self):
        response = requests.get("http://localhost:8080/trace/averageTime", headers={})
        print("averageTimeServer : "+str(response.text))
        return response.text

    def run(self):
        cpu = 0
        memory = 0
        disk = 0
        
        if self.trace:
            monitoring = Monitoring();
            cpu = monitoring.getCpuMean(20);
            memory = monitoring.getMemoryMean(2);
            disk = monitoring.getDiskActiveTimeMean(20)   
        for i in range(0,self.nbThread):
            m = MyThread(i,self.timeDict,self.batchSize,self.nbCreates)
            m.start() 

        while len(self.timeDict.keys())<self.nbThread:
            print("sleep nb :"+str(len(self.timeDict.keys())))
            time.sleep(1)

        totalTime = 0
        for i in range (0,self.nbThread):
            totalTime = totalTime + self.timeDict[i]

        averageTime = totalTime/self.nbThread
        print("average time : "+str(averageTime))
        monitoring = Monitoring();
        print("cpu : "+str(cpu));
        print("memory : "+str(memory));
        print("disk : "+str(disk));
        averageTimeServer = self.getAverageTimeServer()
        if self.trace:
            self.traceExecution(averageTime,averageTimeServer,cpu,memory,disk)
        return {"y" : averageTimeServer,"cpu" : cpu,"memory":memory,"disk":disk}


