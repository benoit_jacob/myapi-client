'''
Created on 16 avr. 2020

@author: b.jacob
'''
from com.bjacob.optim.ParametersSet import ParametersSet
from com.bjacob.optim.Parameter import Parameter
import configparser
from com.bjacob.Orchestrator import Orchestrator
from com.bjacob.api.ApiLauncher import ApiLauncher
from time import sleep
import random
from com.bjacob.SystemRegressor import SystemRegressor
import plotly.graph_objs as go
import csv
import matplotlib.pyplot as plt


class GradientDescent(object):
    
    def __init__(self,maxIterations,parameters=None):
        self.maxIterations = maxIterations
        self.parameters = ParametersSet()
        self.parametersHisto = []
        self.logFile = open('gradient.log', 'w')
        self.addParameters(parameters)
        self.regressor = SystemRegressor();
    
        self.regressor.regressGradientBoost()
        
    def addParameters(self,parameters):
        if parameters is None :
            threadPoolParam = Parameter("server.tomcat.max-threads",1,100,1)
            batchSizeParam = Parameter("spring.jpa.properties.hibernate.jdbc.batch_size",1,100000,100);
            maxPoolSizeParam = Parameter("spring.datasource.hikari.maximumPoolSize",1,100,1)
            self.parameters.addParameter(threadPoolParam)
            self.parameters.addParameter(batchSizeParam)
            self.parameters.addParameter(maxPoolSizeParam)
        else :
            for p in parameters:
                self.parameters.addParameter(p);
        
    def applyParameters(self):
        config = configparser.ConfigParser()
        for param in self.parameters.parameters:
            config['DEFAULT'][param.name] = str(param.currentValue)
            self.log('apply param :'+str(param))
        
        #config['DEFAULT']['server.tomcat.max-threads']='1'
        config['DEFAULT']['spring.datasource.hikari.connectionTimeout']='20000'
        config['DEFAULT']['spring.datasource.url']='jdbc:postgresql://localhost:5432/my-db'
        config['DEFAULT']['spring.datasource.username']='dbuser'
        config['DEFAULT']['spring.datasource.password']='password'
        config['DEFAULT']['spring.jpa.properties.hibernate.dialect']='org.hibernate.dialect.PostgreSQLDialect'
        config['DEFAULT']['spring.jpa.hibernate.ddl-auto']='create'
        config['DEFAULT']['management.endpoints.web.exposure.include']='httptrace,info,health'
        
        with open('application.properties', 'w') as configfile:
            config.write(configfile)
            self.log('config wrote')
            
    def run(self):
        o = Orchestrator()
        a = ApiLauncher()
        
        a.runApi(stop=True)
        sleep(5)
        return o.runClient()
    
    def move(self,theta):
        indexParam = 0
        self.parametersHisto.append(self.parameters.clone())
        for param in self.parameters.parameters:
            oldVal = param.currentValue
            val = round(param.currentValue - theta[indexParam]*param.step)
            if val == param.currentValue:
                if theta[indexParam]>0 :
                    val = param.currentValue - param.step
                else:
                    val = param.currentValue + param.step
            param.setCurrentValue(val)
            if param.currentValue==oldVal:
                self.log('valeur non modifiée !!!!')
                if theta[indexParam]>0 :
                    val = param.currentValue + 2*param.step
                else:
                    val = param.currentValue - 2*param.step
                if val > param.maxValue:
                    val = param.currentValue - 2*param.step
                elif val < param.minValue:
                    val = param.currentValue + 2*param.step 
                param.setCurrentValue(val)
            self.log('param : '+param.name+' ->'+str(param.currentValue)+' oldVal : '+str(oldVal))
            indexParam = indexParam + 1
    
    def getY1(self,dictRun):
        y = float(dictRun['y'])
        cpu = float(dictRun['cpu'])
        memory = float(dictRun['memory'])
        disk = float(dictRun['disk'])
        
    
        loss = self.regressor.getPercentLoss(cpu,memory,disk,plot=False)
        
        y1 = float(y - y*loss)
        self.log('y : {}, loss : {}, y1 : {}'.format(y,loss,y1))
        
        return y1 
        
    def findOptimum(self):
        x0 = self.parameters.parameters
        self.applyParameters()
        dictRun = self.run()
        y0 = self.getY1(dictRun)
        grad = []
        nbParam = len(x0)
        for i in range(0,nbParam):
            grad.append(1)
        xprec = self.parameters.clone().parameters
        yprec = y0
        ymin = y0
        xmin = x0
        delta = 1000
        deltaList = []
        nb = 0
        while abs(delta)>0.1 and nb < self.maxIterations:
            self.log('')
            self.log('------------>iteration : '+str(nb)+' ------------------------------------------------>')
            self.move(grad)
            x1 = self.parameters.parameters
            self.applyParameters()
            dictRun = self.run()
            y1 = self.getY1(dictRun)
            if y1 < ymin :
                ymin = y1
                xmin = self.parameters.clone().parameters
                self.log('ymin : {}'.format(str(ymin)))
            self.log('y1 : {}'.format(y1))
            delta = (y1-yprec)*100/yprec
            deltaList.append(delta)
            self.log('delta : {}'.format(delta))
            self.logVect('xprec',xprec)
            self.logVect('x1',x1)
            alpha = 0.1
            grad = []
            for i in range(0,nbParam):
                grad.append(alpha*(y1-yprec)/(float(x1[i].currentValue)-float(xprec[i].currentValue)))
            self.logVect('grad',grad)
            self.analyzeDelta(deltaList, grad)
            yprec = y1
            xprec = self.parameters.clone().parameters
            nb = nb + 1
            self.flushLog()
        self.log('ymin : {}'.format(str(ymin)))
        print('ymin : {}'.format(str(ymin)))
            
    def analyzeDelta(self,deltaList,grad):
        last = len(deltaList)-1;
        nb = 10
        nbBetter = 0
        if len(deltaList)>nb and deltaList[last]>0:
            for i in range(last, last-nb):
                if grad[i] <0:
                    nbBetter = nbBetter + 1;
            if nbBetter< nb*2/3:
                
                #changement de direction aleatoire
                index = random.randint(0,(len(grad)-1))
                self.log('changement direction : {}'.format(str(index)))
                if grad[index]<0:
                    grad[index] = 1
                else :
                    grad[index] = -1
            
        
    def log(self,text):    
        self.logFile.write(text+'\n')
        
    def flushLog(self):
        self.logFile.close()
        self.logFile = open('gradient.log', 'a')
        
    def logVectX(self,name,vect):
        self.logFile.write(name+': \n')
        for v in vect.parameters:    
            self.logFile.write(str(v)+'\n')
    
    def logVect(self,name,vect):
        self.logFile.write(name+': \n')
        for v in vect:    
            self.logFile.write(str(v)+'\n')
            
    def plot(self,csvFile,paramFixe = None):
        jdbcPool = []
        batchSize=[]
        threadPool = []
        time=[]
        with open(csvFile, newline='') as csvfile1:
            reader = csv.reader(csvfile1, delimiter=';')
            rownum = 0
            for row in reader:
                if rownum>0:
                    jdbcPool.append(row[4])
                    batchSize.append(row[5])
                    threadPool.append(row[6])
                    cpu = row[9]
                    mem = row[10]
                    disk = row[11]
                    y = float(row[8])
                    print('y : {}'.format(str(y)))
                    loss = float(self.regressor.getPercentLoss(cpu,mem,disk,plot=False))
                    print('loss : {}'.format(str(loss)))
                    y1 = 1/float(y - y*loss)
                    time.append(y1)
                rownum = rownum+1
        
        x1 = jdbcPool
        y1 = batchSize
        z1 = threadPool
        if paramFixe is not None:
            z1 = time;
            if paramFixe == 'jdbcPool':
                x1 = threadPool
            elif paramFixe == 'batchSize':
                y1 = threadPool
        fig = go.Figure(data=[go.Scatter3d(x=x1,
                  y=y1,z=z1,mode='markers',
                  marker=dict(
        size=12,
        color=time,
        colorscale='Viridis',   # choose a colorscale
        opacity=0.8
    )
                  )])
       
        fig.show()    
      
    

if __name__ == '__main__':
    
    threadPoolParam = Parameter("server.tomcat.max-threads",1,100,1)
    batchSizeParam = Parameter("spring.jpa.properties.hibernate.jdbc.batch_size",280,100000,100);
    maxPoolSizeParam = Parameter("spring.datasource.hikari.maximumPoolSize",85,100,1)
    params = [threadPoolParam,batchSizeParam,maxPoolSizeParam]
    gd = GradientDescent(500,parameters=params)
    gd.applyParameters()
    dictRun = gd.run()
    
    y1 = gd.getY1(dictRun)
    print('y1 : {}'.format(str(y1)))    
   
    #gd = GradientDescent(500)
    #gd.findOptimum()
    
    
    #gd.plot('traces/gradient-1000-50/executions.csv')
    #gd.plot('traces/gradient-1000-50/threadPool-1/executions.csv',paramFixe='threadPool')
    #gd.plotLines('traces/gradient-1000-50/threadPool-2/executions.csv')
    
    pass