'''
Created on 3 mars 2020

@author: b.jacob
'''

import requests
import json
import threading
import time

baseUrl = "http://localhost:8080/product"


class MyThread (threading.Thread):
    def __init__(self, index,timeDict,batchSize,nbCreates):      # jusqua = donnée supplémentaire
        threading.Thread.__init__(self)  # ne pas oublier cette ligne
        # (appel au constructeur de la classe mère)
        self.index = index
        self.timeDict = timeDict;
        self.batchSize = batchSize;
        self.nbCreates = nbCreates;

    def getAll(self):
        response = requests.get(baseUrl+"/getAll", headers={})

        print (response.json())
        
    def search(self, name):
        #print('search : ',name)
        response = requests.get(baseUrl+"/search/"+name, headers={})

        #print ('searche response:',response.json())

    def create(self,threadIndex):
        for j in range(0,self.nbCreates):
            json = '['
            maxi = self.batchSize
            for i in range (0,maxi):
                json = json+'{"name" : "product'+str(j)+'_'+str(i)+'","price": 1'+str(i+j)+',"info1": "hello_'+str(self.index)+'","info2": "hello2","info3": "hello3","info4": "hello4","info5": "hello5"}'
                if i<(maxi-1):
                    json = json +','
                else:
                    json = json+']'
                #print(json)
            response = requests.post(baseUrl+"/createAll",data=json,headers={'Content-type': 'application/json'})
            print(response)

    def run(self):
        
        print("thread :", self.index)
        start = time.time()
        
        self.create(self.index)
        
        for i in range(0,self.nbCreates): 
            self.search('product'+str(self.index)+'_'+str(i))
        end = time.time()
        delay = (end - start)
        
        print("time : "+str(delay)+" for thread : "+str(self.index))
        self.timeDict[self.index] = delay
        time.sleep(0.08)   # attend 100 millisecondes sans rien faire
            # facilite la lecture de l'affichage