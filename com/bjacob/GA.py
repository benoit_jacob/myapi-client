'''
Created on 12 mai 2020

@author: b.jacob
'''


import csv
from com.bjacob.SystemRegressor import SystemRegressor
from builtins import id
from com.bjacob.Orchestrator import Orchestrator
from com.bjacob.api.ApiLauncher import ApiLauncher
from time import sleep
import configparser
import random

class Chromosome(object):
    
    def __init__(self,id,values,y1):
        self.id = id
        self.values = values
        self.y1 = y1
        
    def crossover(self,chrom,cross_point):
        nbParams = len(self.values)
        values = (self.values[0:cross_point +1] +chrom.values[cross_point+1:nbParams])
        return Chromosome(self.id+'-'+chrom.id,values,None)
    
    def __str__(self):
        return str(self.values) + "->"+str(self.y1) 

class GA(object):
    '''
    classdocs
    '''


    def __init__(self,fileInit,nbParams,bornes,nbIterations,paramNames):
        self.fileInit = fileInit
        self.dict = {}
        self.paramNames = paramNames
        self.logFile = open('ga.log', 'w')
        self.parents = []
        self.nbParams = nbParams;
        self.bornes = bornes
        self.nbIterations = nbIterations
        self.regressor = SystemRegressor();
        self.regressor.regressGradientBoost()
        self.initializePopulation()
        
        
        '''
        Constructor
        '''
    def log(self,text):    
        self.logFile.write(str(text)+'\n')
        
    def flushLog(self):
        self.logFile.close()
        self.logFile = open('ga.log', 'a')
    
    def initializePopulation(self):
        self.populations = []
        with open(self.fileInit, 'r') as csvfile1:
            reader = csv.reader(csvfile1, delimiter=';')
            
            rownum = 0
            chromosomes = []
            for row in reader:
                if rownum>0:
                    id = row[0]
                    jdbcPool=float(row[4])
                    batchSize = float(row[5])
                    threadPool=float(row[6])
                    y1 = float(row[12])
                    values = []
                    values.append(jdbcPool)
                    values.append(batchSize)
                    values.append(threadPool)
                    chromosome = Chromosome(id,values,y1)
                    chromosomes.append(chromosome)
                    
                rownum=rownum+1
            chromosomes = sorted(chromosomes, key=lambda chromosome: chromosome.y1,reverse=False)
            self.log('chromosomes : '+str(len(chromosomes)))
            nb = int(len(chromosomes)/4)
            for i in range(0,nb):
                self.populations.append(chromosomes[i])
    
        self.log('populations : ')
        for p in self.populations:
            print(p)
        self.best = 10000
        self.parents = []
        
    def getY1(self,dictRun):
        y = float(dictRun['y'])
        cpu = float(dictRun['cpu'])
        memory = float(dictRun['memory'])
        disk = float(dictRun['disk'])
        
    
        loss = self.regressor.getPercentLoss(cpu,memory,disk,plot=False)
        
        y1 = float(y - y*loss)
        self.log('y : {}, loss : {}, y1 : {}'.format(y,loss,y1))
        
        return y1
    
    def run(self):
        for i in range(0,self.nbIterations):
            self.log('iteration : '+str(i)+'----------------------------------------------------------------------')
            self.fitness_score()
            self.selectparent()
            self.crossover()
            self.mutate()
            self.flushLog()
            
    def applyParameters(self):
        config = configparser.ConfigParser()
        chromosome = self.populations[len(self.populations)-1]
        for i in range(0,self.nbParams):
            config['DEFAULT'][self.paramNames[i]] = str(int(chromosome.values[i]))
            self.log('apply param :'+str(self.paramNames[i]+'->'+str(chromosome.values[i])))
        
        #config['DEFAULT']['server.tomcat.max-threads']='1'
        config['DEFAULT']['spring.datasource.hikari.connectionTimeout']='20000'
        config['DEFAULT']['spring.datasource.url']='jdbc:postgresql://localhost:5432/my-db'
        config['DEFAULT']['spring.datasource.username']='dbuser'
        config['DEFAULT']['spring.datasource.password']='password'
        config['DEFAULT']['spring.jpa.properties.hibernate.dialect']='org.hibernate.dialect.PostgreSQLDialect'
        config['DEFAULT']['spring.jpa.hibernate.ddl-auto']='create'
        config['DEFAULT']['management.endpoints.web.exposure.include']='httptrace,info,health'
        
        with open('application.properties', 'w') as configfile:
            config.write(configfile)
            self.log('config wrote')
        
    def runAppli(self):
        sleep(5)
        o = Orchestrator()
        a = ApiLauncher()
        
        a.runApi(stop=True)
        sleep(5)
        return o.runClient()
    
    def fitness_score(self):
        for chrom in self.populations:
            if chrom.y1 is None:
                self.applyParameters()
                dictRun = self.runAppli()
                y1 = self.getY1(dictRun)
                chrom.y1 = y1
        self.populations=sorted(self.populations, key=lambda chromosome: chromosome.y1)
        self.populations = self.populations[0:10]
        self.log('populations fitness : ')
        for p in self.populations:
            self.log(p)
        
    def selectparent(self):
        index1 = 0
        index2 = 1
        size = len(self.populations)
        if size>=4:
            index1 = random.randint(0,size-1)
            if index1< (size -1):
                index2 = random.randint(index1+1,size-1)
            else:
              index2 = random.randint(0,size-2)  
        self.parents = []
        self.parents.append(self.populations[index1])
        self.parents.append(self.populations[index2])
    
    def crossover(self):
        cross_point = random.randint(0,self.nbParams-1)
        self.populations.append(self.parents[0].crossover(self.parents[1],cross_point))
        self.log('parents crossover: ')
        for p in self.parents:
            self.log(p)
                
    def mutate(self):
        indexMutate = len(self.populations)-1
        mutate_point = random.randint(0,self.nbParams-1)
        mutate_pourcent = random.randint(0,20)
        mutate_sign = random.randint(0,1)
        if len(self.populations)<= indexMutate:
            self.log('parents len : '+str(len(self.populations)))
        elif len(self.populations[indexMutate].values)<= mutate_point:
            self.log('values len : '+str(len(self.populations[indexMutate].values)))
        val = self.populations[indexMutate].values[mutate_point]
        oldval = val
        self.log('mutate_point : '+str(mutate_point))
        self.log('mutate val : '+str(val))
        if mutate_sign == 0:
            val = int(val + val*mutate_pourcent/100)
            if val == oldval:
                val = val + 1;
        else :
            val = int(val - val*mutate_pourcent/100)
            if val == oldval:
                val = val -1;
        bornes = self.bornes[mutate_point]
        if val > bornes[1]:
            val = int(bornes[1] - val*mutate_pourcent/100)
        if val < bornes[0]:
            val = int(bornes[0] + val*mutate_pourcent/100)
        self.populations[indexMutate].values[mutate_point] = val
        #self.populations = self.parents
        self.log('populations mutate : ')
        for p in self.populations:
            self.log(p)
        
        
        
    
if __name__ == '__main__':
    ga = GA('./traces/gradient-1000-50/executions-c.csv',3,[[1,100],[1,100000],[1,100]],100,['spring.datasource.hikari.maximumPoolSize','spring.jpa.properties.hibernate.jdbc.batch_size','server.tomcat.max-threads']);
    ga.run()
    
    