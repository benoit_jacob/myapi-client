#python -m pip install requests


from com.bjacob.MyApiClient import MyApiClient
from time import sleep
from com.bjacob.api.ApiLauncher import ApiLauncher


class Orchestrator:
    def __init__(self):
        self.apiLauncher = ApiLauncher()
        print('new Orchestrator ...')

    def runClient(self):
        print('echauffement ...')
        client = MyApiClient(5,False)
        client.run()
        print('tir')
        client = MyApiClient(50,True)
        return client.run()
        
    def runApi(self):
        ok = self.apiLauncher.runApi()
        maxRetry = 5
        i = 0
        
        while i<maxRetry and not ok:
            i = i+1
            self.apiLauncher.stopApi()
            ok = self.apiLauncher.runApi()
        
    def stopApi(self):
        self.apiLauncher.stopApi()
       

    
if __name__ == '__main__':
    for i in range(0,1):
        o = Orchestrator()
        o.runApi()
        sleep(10)
        o.runClient()
        o.stopApi()
        sleep(20)
    
