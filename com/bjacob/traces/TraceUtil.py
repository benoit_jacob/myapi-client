'''
Created on 12 mai 2020

@author: b.jacob
'''

import os
import shutil
import csv
from com.bjacob.SystemRegressor import SystemRegressor

class TraceUtil(object):
    '''
    classdocs
    '''


    def __init__(self):
        self.regressor = SystemRegressor();
    
        self.regressor.regressGradientBoost()
        '''
        Constructor
        '''
    

    def complete(self,csvFile):
        jdbcPool = []
        batchSize=[]
        threadPool = []
        time=[]
        iterations = []
        newFileName = csvFile.replace('.csv','-c.csv')
        
        rows = []
        
        with open(csvFile, 'r') as csvfile1:
            reader = csv.reader(csvfile1, delimiter=';')
            
            rownum = 0
            for row in reader:
                if rownum>0:
                    print("rownum : "+str(rownum))
                    jdbcPool.append(float(row[4]))
                    batchSize.append(float(row[5])/4)
                    threadPool.append(float(row[6]))
                    cpu = row[9]
                    mem = row[10]
                    disk = row[11]
                    y = float(row[8])
                    print('y : {}'.format(str(y)))
                    loss = float(self.regressor.getPercentLoss(cpu,mem,disk,plot=False))
                    print('loss : {}'.format(str(loss)))
                    y1 = float(y - y*loss)
                    print('y1 : {}'.format(str(y1)))
                    time.append(y1)
                    if y1 >500:
                        print('valeur bizarre : '+str(y1)+'- rownum : '+str(rownum))
                    iterations.append(int(rownum))
                    row.append(y1);
                    
                else :
                    row.append('y1')
                    
                rownum = rownum+1
                rows.append(row)
            
    
            with open(newFileName, 'w',newline='') as csvfile1:
                 writer = csv.writer(csvfile1,delimiter=';')
                 writer.writerows(rows)    
                
    
    
if __name__ == '__main__':
    traceUtil = TraceUtil();
    
    traceUtil.complete('./ga/executions.csv')