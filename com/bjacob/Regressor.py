'''
Created on 20 mars 2020

@author: b.jacob
'''
from asyncio.subprocess import STDOUT
import csv
from subprocess import Popen, PIPE
import subprocess
import sys
from time import sleep

from backcall._signatures import Parameter
import plotly
from sklearn import linear_model
from sklearn import tree
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.manifold import MDS
from xgboost import XGBRegressor

from com.bjacob.Orchestrator import Orchestrator
from com.bjacob.api.ApiLauncher import ApiLauncher
import matplotlib.pyplot as plt
import numpy as np
import plotly.graph_objs as go


        
class Model:
    def __init__(self,csvFileList):
        self.X = []
        self.Y = []
        for csvFile in csvFileList:
            self.parse(csvFile)
       
    def parse(self,csvFile):
        with open(csvFile, 'r') as csvfile1:
            reader = csv.reader(csvfile1, delimiter=';')
            
            rownum = 0
            for row in reader:
                if rownum>0:
                    self.X.append([float(row[4]),float(row[5]),float(row[6])])
                   
                    self.Y.append(float(row[12]))
                    
                    
                rownum = rownum+1
                
    def split(self,nb=None):
        if nb is None:
            nb = int(self.X.__len__()/4)
        print('nb elements test: '+str(nb))
        
        print('nb elements total: '+str(self.X.__len__()))
        self.X0 = self.X[(self.X.__len__() - nb):self.X.__len__()]
        self.Y0 = self.Y[(self.Y.__len__() - nb):self.Y.__len__()]
        for i in range((self.X.__len__() - nb),self.X.__len__()):
            self.X.pop(self.X.__len__()-1)
            self.Y.pop(self.Y.__len__()-1)
  

class Regressor(object):
    '''
    classdocs
    '''


    def __init__(self,csvFileList):
        '''
        Constructor
        '''
        self.model = Model(csvFileList)
    
        
    def regressLinear(self):
        reg = linear_model.LinearRegression()
        model = self.getSystemModel()
        model.split(20)
        reg.fit(model.X, model.Y);
        print(model.X0)
        y1 = reg.predict(model.X0)
        print("coeff : ")
        print(reg.coef_)
        print("y1 = "+str(y1))
        mse = np.sum(abs(y1 - model.Y0)/model.Y0)
        rmse = (mse/model.Y0.__len__())*100
        print("error % : "+str(rmse))
        return model;
        
    def regressTree(self):
        reg = tree.DecisionTreeRegressor()
        model = self.getSystemModel()
        model.split(20)
        reg.fit(model.X, model.Y);
        print(model.X0)
        y1 = reg.predict(model.X0)
        
        print("y1 = "+str(y1))
        mse = np.sum(abs(y1 - model.Y0)/model.Y0)
        rmse = (mse/model.Y0.__len__())*100
        print("error % : "+str(rmse))
        return model;
        
    def regressGradientBoost(self):
        reg = GradientBoostingRegressor(n_estimators=100, learning_rate=0.1, max_depth=1, random_state=0, loss='lad')
        model = self.model
        model.split()
        reg.fit(model.X, model.Y);
        print(model.X0)
        y1 = reg.predict(model.X0)
        
        print("y1 = "+str(y1))
        mse = np.sum(abs(y1 - model.Y0)/model.Y0)
        rmse = (mse/model.Y0.__len__())*100
        print("error % : "+str(rmse))
        return model;
        
    def regressXGBoost(self):
        reg = XGBRegressor(objective="reg:squarederror")
        model = self.getSystemModel()
        model.split(200)
        reg.fit(model.X, model.Y);
        print(model.X0)
        y1 = reg.predict(model.X0)
        
        print("y1 = "+str(y1))
        mse = np.sum(abs(y1 - model.Y0)/model.Y0)
        rmse = (mse/model.Y0.__len__())*100
        print("error % : "+str(rmse))
        return model;
    
    def plot(self,model):
        cpuTab = []
        memoryTab=[]
        diskTab = []
        for vect in model.X:
            cpuTab.append(vect[0])
            memoryTab.append(vect[1])
            diskTab.append(vect[2])
            
        fig = go.Figure(data=[go.Scatter3d(x=cpuTab,
                  y=memoryTab,z=diskTab,mode='markers',
                  marker=dict(
        size=12,
        color=model.Y,
        colorscale='Viridis',   # choose a colorscale
        opacity=0.8
    )
                  )])
        
        fig.show()

        
   
        
    
        
if __name__ == '__main__':
    
    #ga
    #regressor = Regressor(['./traces/ga/executions-c.csv','./traces/ga/initMauvaise/executions-c.csv']);
    
    #gradien
    regressor = Regressor(['./traces/gradient-1000-50/executions-c.csv','./traces/gradient-1000-50/145iterations/executions-c.csv',
                           './traces/gradient-1000-50/threadPool-1/executions-c.csv','./traces/gradient-1000-50/threadPool-2/executions-c.csv']);
    
    #regressor.regressLinear()
    #regressor.regressTree()
    regressor.regressGradientBoost()
    #regressor.regressXGBoost()
    #regressor.run()
    
    sys.exit(0)
        