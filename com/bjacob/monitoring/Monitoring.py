#python -m pip install requests


import subprocess

class Monitoring:
    def __init__(self):
        print('monitoring ...')

    
    def getCpu(self):
       cpuStr = subprocess.check_output("wmic cpu get loadpercentage").decode("utf-8");
       lines = cpuStr.splitlines();
       nb = 0;
       if len(lines)>2:
           nb = lines[2]
      
       return nb;
   
    def getCpuMean(self,nbCalls):
       sum = 0;
       for i in range(0,nbCalls):
           try :
               sum = sum + int(self.getCpu())
           except :
               print("exception cpu")
       return sum/nbCalls
   
    def getMemory(self):
       memStr = subprocess.check_output("wmic OS get FreePhysicalMemory").decode("utf-8");
       lines = memStr.splitlines();
       nb = 0;
       if len(lines)>2:
           nb = lines[2]
      
       return nb;
       
   
    def getMemoryMean(self,nbCalls):
       sum = 0;
       for i in range(0,nbCalls):
           try : 
               sum = sum + int(self.getMemory())
           except :
               print('error in memory')
       return sum/nbCalls
   
    def getDiskActiveTimeMean(self,nbCalls):
       sum = 0.0;
       for i in range(0,nbCalls):
           try : 
               sum = sum + self.getDiskActiveTime()
           except :
               print('error in disk')
       return sum/nbCalls
   
    def getDiskActiveTime(self):
        diskStr = subprocess.check_output('powershell -ExecutionPolicy ByPass -Command Get-WmiObject -class Win32_PerfFormattedData_PerfDisk_LogicalDisk').decode("utf-8");
        print(diskStr)
        percentDiskTime = -1;
        lines = diskStr.splitlines();
        line = ''
        i = 0
        while 'PercentIdleTime' not in line and i<len(lines):
            line = lines[i]
            i=i+1
        tab = line.split(':')
        if len(tab)>1:
            percentDiskTime = (1- int(tab[1])/100)*100
        return percentDiskTime
        
if __name__ == '__main__':
    moni = Monitoring()
   
    print("disk active time:"+str(moni.getDiskActiveTime()))
    print("cpu:"+str(moni.getCpu()))
    print("memory : "+str(moni.getMemory()))
    print("cpu mean:"+str(moni.getCpuMean(5)))
    print("memory mean: "+str(moni.getMemoryMean(5)))
        
   
   
        
       

    
    
