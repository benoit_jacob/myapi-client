'''
Created on 12 mai 2020

@author: b.jacob
'''

import plotly.graph_objects as go
import csv
from plotly.subplots import make_subplots
import pandas as pd
import numpy as np

class Plot3DSurface(object):
    '''
    classdocs
    '''


    def __init__(self):
        
        '''
        Constructor
        '''
    
    def test(self):
        import plotly.graph_objects as go
        import pandas as pd
        import numpy as np
# Read data from a csv
        z_data = pd.read_csv('https://raw.githubusercontent.com/plotly/datasets/master/api_docs/mt_bruno_elevation.csv')
        z = z_data.values
        sh_0, sh_1 = z.shape
        x, y = np.linspace(0, 1, sh_0), np.linspace(0, 1, sh_1)
        fig = go.Figure(data=[go.Surface(z=z, x=x, y=y)])
        fig.update_layout(title='Mt Bruno Elevation', autosize=False,
                  width=500, height=500,
                  margin=dict(l=65, r=50, b=65, t=90))
        fig.show()
        
    def plot(self,csvFile):
        jdbcPool = []
        batchSize=[]
        threadPool = []
        time=[]
        iterations = []
        dictMat = {}
        with open(csvFile, newline='') as csvfile1:
            reader = csv.reader(csvfile1, delimiter=';')
            rownum = 0
            for row in reader:
                if rownum>0:
                    jdbcPool.append(int(row[4]))
                    batchSize.append(int(row[5]))
                    threadPool.append(int(row[6]))
                    
                    cpu = row[9]
                    mem = row[10]
                    disk = row[11]
                    y = float(row[8])
                    
                    time.append(float(row[12]))
                    dictMat[str(row[4])+"-"+str(row[5])]=1000/float(row[12])
                   
                    iterations.append(int(rownum))
                rownum = rownum+1
                
        #
        #jdbcPool = [30,5,55,45,100]
        #jdbcPool=[1, 1, 3, 14, 9, 12, 18, 15, 13, 11, 1, 3, 11, 12, 22, 26, 28, 29, 40, 41, 44, 45, 54, 53, 57, 54, 53, 52, 51, 50, 89, 91, 100, 98, 94, 100, 98, 100, 98, 97, 92, 91, 84, 83, 77, 79, 82, 80, 79, 69, 68, 61, 62, 67, 66, 70, 72, 68, 67, 66, 59, 60, 67, 68, 74, 75, 80, 79, 84, 85, 80, 83, 84, 89, 92, 97, 95, 94, 100, 97, 88, 86, 100, 98, 90, 89, 77, 78, 85, 84, 76, 77, 62, 61, 60, 36, 35, 34, 1, 3, 15, 16, 17, 1, 3, 37, 38, 39, 45, 44, 45, 48, 50, 47, 46, 36, 37, 42, 41, 46, 48, 49, 57, 56, 48, 49, 42, 40, 39, 34, 36, 34, 36, 37, 46, 47, 40, 41, 53, 52, 45, 46, 42, 44, 52, 51, 43, 44, 49, 48, 63, 64, 65, 55]

        #batchSize = [100,50,250,150,300]  
        #time = [348,500,268,415,512]
        
        x, y,z = np.linspace(0, 1, len(time)), np.linspace(0, 1, len(time)),np.linspace(0, 1, len(time))
        mat = []
        for i in range (0,len(time)):
            line = [];
            for j in range (0,len(time)):
                key = str(jdbcPool[i])+"-"+str(batchSize[j])
                val = 0
                if key in dictMat:
                    val = dictMat[key]
                line.append(val)
            mat.append(line)
            
        for j in range(0,len(jdbcPool)):
            x[j] = jdbcPool[j]
            
        for j in range (0,len(batchSize)):
            y[j] = batchSize[j]
        
        for j in range (0,len(time)):
            z[j] = 1/time[j]*100             
                
        dict1 = {'jdbcPool': jdbcPool, 'batchSize': batchSize, 'time': time}  
    
        df = pd.DataFrame(dict1)
                
        print('time : ')        
        print(str(time))
        
        print('jdbcPool : ')        
        print(jdbcPool)
        
        print('batchSize : ')        
        print(len(batchSize))
        
        print('threadPool : ')        
        print(threadPool)
        
        print('df.jdbcpool : ')
        print(df['jdbcPool'])
        
        print('df.batchSize : ')
        print(df['batchSize'])
        
        print('df.iloc 0 ')
        print(df.iloc[:,0].values)
        xaxis = dict(range=[0,200],title='jdbcPool',dtick=50, autorange=False)
        
        layout = go.Layout(
    title="Title",
    xaxis=dict(
        title="X Label"
    ),
    yaxis=dict(
        title="Y label"
    ) ) 
        
        trace = go.Surface(x=x, y=y, z=mat)
        print('trace.z : ')
        print(trace.z)
        
        fig = go.Figure(data=[trace],layout=layout)
        
        xprops = {
    'title': {
      'text': 'jdbc Pool',
      'font': {
        'family': 'Courier New, monospace',
        'size': 24,
        'color': '#7f7f7f'
      }
    }
  }

        
        #fig.update_layout(title='Time', autosize=False,xaxis=xaxis ,
         #         width=500, height=500,
          #        margin={'l':65, 'r':50, 'b':65, 't':90})
    

        fig.show()
    
    
    
if __name__ == '__main__':
    plot = Plot3DSurface();
    plot.plot('../traces/ga/initMauvaise/executions-c.csv')
    #plot.test()
    
    