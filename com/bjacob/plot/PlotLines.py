'''
Created on 12 mai 2020

@author: b.jacob
'''

import matplotlib.pyplot as plt

import numpy as np
import plotly.graph_objs as go
import csv
from com.bjacob.SystemRegressor import SystemRegressor

class PlotLines(object):
    '''
    classdocs
    '''


    def __init__(self):
        self.regressor = SystemRegressor();
    
        self.regressor.regressGradientBoost()
        '''
        Constructor
        '''
    
    def test(self):
        x = np.linspace(0, 10, 1000)
        fig, ax = plt.subplots()
        ax.plot(x, np.sin(x), '-b', label='Sine')
        ax.plot(x, np.cos(x), '--r', label='Cosine')
        ax.axis('equal')
        leg = ax.legend();
        plt.show()
    
    def plotLines(self,csvFile):
        jdbcPool = []
        batchSize=[]
        threadPool = []
        time=[]
        iterations = []
        with open(csvFile, newline='') as csvfile1:
            reader = csv.reader(csvfile1, delimiter=';')
            rownum = 0
            for row in reader:
                if rownum>0:
                    jdbcPool.append(float(row[4]))
                    batchSize.append(float(row[5])/4)
                    threadPool.append(float(row[6]))
                    cpu = row[9]
                    mem = row[10]
                    disk = row[11]
                    y = float(row[8])
                    print('y : {}'.format(str(y)))
                    loss = float(self.regressor.getPercentLoss(cpu,mem,disk,plot=False))
                    print('loss : {}'.format(str(loss)))
                    y1 = float(y - y*loss)/4
                    time.append(y1)
                    if y1 >500:
                        print('valeur bizarre : '+str(y1)+'- rownum : '+str(rownum))
                    iterations.append(int(rownum))
                rownum = rownum+1
                
        print('time : ')        
        print(time)
        
        print('jdbcPool : ')        
        print(jdbcPool)
        
        print('batchSize : ')        
        print(batchSize)
        
        print('threadPool : ')        
        print(threadPool)
        
        fig, ax = plt.subplots()
        
        ax.plot(iterations, jdbcPool, '-b', label='BD Pool')
        ax.plot(iterations, batchSize, '--r', label='Batch Size')
        ax.plot(iterations, threadPool, '-g', label='Thread Pool')
        ax.plot(iterations, time, color='orange', label='Time',marker='o')
        
        #ax.axis('equal')
        
        leg = ax.legend();
        plt.show()
    
if __name__ == '__main__':
    plotLines = PlotLines();
    
    plotLines.plotLines('../traces/gradient-1000-50/executions.csv')