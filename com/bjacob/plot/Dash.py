'''
Created on 12 mai 2020

@author: b.jacob
'''

import matplotlib.pyplot as plt

import numpy as np
import plotly.graph_objs as go
import csv
from com.bjacob.SystemRegressor import SystemRegressor

import dash
#import dash_bootstrap_components as dbc      #for columns/tabs
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State
import plotly.graph_objs as go    #for making plot.ly graph objects

def global_function_01(inputs):
    return inputs + 1

global_variable_01 = [1,2,3,5,7,11,13,17]
app = dash.Dash(__name__)

app.layout = html.Div(children=[  ### whole page
    html.H1('Title of Your Application',   ### page header
        style={'textAlign': 'center', 'margin-bottom': 0.3}
    ),
    html.Br(),
    html.Div(id='graphs', children=[     ### page contents
        dcc.Tabs(id='tabs', 
            value='tab-1', 
            style={
                'height': '45px'
            },
            children=[                  ### tabs for the page
                dcc.Tab(id='tab1', value='tab-1', 
                    style={
            'textAlign': 'center',
            'color': '#7FDBFF' }
        , 
                    selected_style={
            'textAlign': 'center',
            'color': '#7FDBFF' }),
                dcc.Tab(id='tab2', value='tab-2', 
                    style={
            'textAlign': 'center',
            'color': '#7FDBFF' }, 
                    selected_style={
            'textAlign': 'center',
            'color': '#7FDBFF' }),
            ]
        ),
        ### displayed below tabs ###
        html.Div(id='tabs-content', children=[
            dcc.Graph(id='graph'),    #### all graphs 
        ])
    ]),
])
server = app.server

@app.callback(
    [Output('graph', 'figure')],
    [Input('dropdown1', 'value'),
     Input('dropdown2', 'value')])
def update_graph(selected_value1, selected_value2):
    traces = []
    traces.append(go.Scatter(
        x=selected_value1,
        y=selected_value2))
    figure = {'data': traces}
    return figure
# automatically update HTML display if a change is made to code
if __name__ == '__main__':
    app.run_server(debug=True)  #False hides errors from users