'''
Created on 16 avr. 2020

@author: b.jacob
'''

class ParametersSet(object):
    
    def __init__(self):
        self.parameters = []
        self.parametersDict = {}
        
    def clone(self):
        pClone = ParametersSet()
        for p in self.parameters:
            pClone.addParameter(p.clone())
        return pClone
    
    def addParameter(self,param):
        self.parameters.append(param)
        self.parametersDict[param.name] = param    

