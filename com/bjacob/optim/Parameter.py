'''
Created on 16 avr. 2020

@author: b.jacob
'''

class Parameter(object):
    
    def __init__(self,name,minValue, maxValue,step):
        self.name = name
        self.minValue = minValue
        self.maxValue = maxValue
        self.step = step
        self.currentValue = minValue
        
    def clone(self):
        pClone = Parameter(self.name,self.minValue,self.maxValue,self.step)
        pClone.currentValue = self.currentValue 
        return pClone
        
    def setCurrentValue(self,value):
        self.currentValue = min(max(value,self.minValue),self.maxValue)
        
        
    def __str__(self):
        return self.name + " ->" + str(self.currentValue)

